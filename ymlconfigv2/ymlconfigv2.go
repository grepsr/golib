package ymlconfigv2

import (
	"os"
	"strings"

	log "bitbucket.org/grepsr/golib/logger"

	cnf "github.com/olebedev/config"
)

const maxConfigDepth = 3

var conf *cnf.Config

// Get value for a key.
func Get(key string, args ...interface{}) string {
	defaultValue, logKey := processArgs(args)
	count := strings.Count(key, ".") + 1
	if count > maxConfigDepth {
		log.Warn("Simplify config setting '%s' by limiting depth to %d levels from current depth of %d.",
			key,
			maxConfigDepth,
			count,
		)
	}

	// First try getting the value from the environment.
	ret := getFromEnv(key)
	var err error
	if ret == "" {
		ret, err = conf.String(key)
		if err != nil {
			log.Error(err.Error())
			ret = defaultValue
		}
	}

	if logKey {
		log.Info("Getting config; key: %s, value: %s, default: %s.", key, ret, defaultValue)
	}
	return ret
}

// Initialize is used for setting things up.
func Initialize(configFile string) {
	parseConfig(configFile)
}

// parseConfig parses the yaml config.
func parseConfig(configStr string) {
	c, err := cnf.ParseYaml(configStr)
	if err != nil {
		log.Error("Could not parse config.")
		log.Error(err.Error())
	}
	conf = c
}

// processArgs processes the arguments passed to Get.
func processArgs(args []interface{}) (defaultValue string, logKey bool) {

	switch len(args) {
	case 0:
		break
	case 1:
		defaultValue, _ = args[0].(string)
	case 2:
		defaultValue, _ = args[0].(string)
		logKey, _ = args[2].(bool)
	}

	return
}

// getFromEnv returns the config value from the environment (if present).
// Example key: a.b.c, environment variable: CFG_A_B_C
func getFromEnv(key string) string {
	value := ""
	envKey := "CFG"
	ret := strings.Split(key, ".")
	if len(ret) > 0 {
		for i := 0; i < len(ret); i++ {
			envKey += "_" + strings.ToUpper(ret[i])
		}
		value = os.Getenv(envKey)
	}
	return value
}
