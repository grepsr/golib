package ymlconfig_test

import (
	"testing"

	log "bitbucket.org/grepsr/golib/logger"
	cfg "bitbucket.org/grepsr/golib/ymlconfig"
	"github.com/gobuffalo/packr"
)

func TestConfig(t *testing.T) {
	box := packr.NewBox("./config")
	cfg.Initialize(*box)
	ret := cfg.Get("bidhan.test.new")
	log.Info(ret)
}
