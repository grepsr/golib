package event

import (
	"encoding/base64"
	"encoding/json"
	"regexp"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"

	log "bitbucket.org/grepsr/golib/logger"
	uuid "github.com/satori/go.uuid"
)

// Event ...
type Event struct {
	Client      *sns.SNS
	AggregateID string
	TopicARN    string
}

// New creates and returns a new Event type.
func New(awsAccessKeyID string, awsSecretAccessKey string, awsRegion string, topicArn string) *Event {
	sess, err := session.NewSession(&aws.Config{
		Region:      &awsRegion,
		Credentials: credentials.NewStaticCredentials(awsAccessKeyID, awsSecretAccessKey, ""),
	})
	if err != nil {
		log.Error("Could not create session")
		log.Error(err.Error())
		return nil
	}

	snsClient := sns.New(sess)

	u4, err := uuid.NewV4()
	if err != nil {
		log.Error("Could not generate UUID.")
		log.Error(err.Error())
		return nil
	}

	event := Event{
		Client:      snsClient,
		AggregateID: u4.String(),
		TopicARN:    topicArn,
	}

	return &event

}

// Emit an event.
func (e *Event) Emit(
	actor string, teamID int, eventType string, messageType string,
	data map[string]interface{}, ttl int32, source string, queryID string) (*sns.PublishOutput, error) {

	eventCategory := getEventCategory(eventType)
	marshalledData, err := json.Marshal(data)
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}

	u4, err := uuid.NewV4()
	if err != nil {
		log.Error("Could not generate UUID.")
		log.Error(err.Error())
		return nil, err
	}

	publishedOn := time.Now().Format("2006-01-02T15:04:05.000Z")

	meta := map[string]interface{}{
		"actor":        actor,
		"published_on": publishedOn,
		"source":       source,
	}

	if teamID != 0 {
		meta["team_id"] = teamID
	}

	// Only handle JSON message type for now.
	payload := map[string]interface{}{
		"type":         eventType,
		"id":           u4.String(),
		"aggregate_id": e.AggregateID,
		"query_id":     queryID,
		"data":         string(marshalledData),
		"meta":         meta,
		"ttl":          ttl,
	}

	marshalledPayload, err := json.Marshal(payload)
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}

	// Serialize using base64
	serializedPayload := base64.StdEncoding.EncodeToString([]byte(string(marshalledPayload)))

	input := sns.PublishInput{
		TopicArn:         aws.String(e.TopicARN),
		MessageStructure: aws.String("string"),
		Message:          aws.String(serializedPayload),
		MessageAttributes: map[string]*sns.MessageAttributeValue{
			"event_type": &sns.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(eventType),
			},
			"event_category": &sns.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(eventCategory),
			},
			"message_type": &sns.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(messageType),
			},
		},
	}
	output, err := e.Client.Publish(&input)
	return output, err
}

func getEventCategory(eventType string) string {
	r, _ := regexp.Compile("^([A-Z][a-z]+)([A-Z][a-z]+)?[A-Z][a-z]+edEvent+$")
	result := r.FindStringSubmatch(eventType)
	if len(result) > 1 {
		return result[1]
	}
	return ""
}
