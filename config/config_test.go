package config_test

import (
	"testing"
	"time"

	cfg "bitbucket.org/grepsr/golib/config"
	log "bitbucket.org/grepsr/golib/logger"
)

func TestConfig(t *testing.T) {
	hosts := []string{"192.168.88.5:2181"}
	path := "/grepsr/dev/email-service"
	cfg.Initialize(hosts, path)
	ret := cfg.Get("profile.grepsr.from_email")
	log.Info(ret)
	time.Sleep(time.Second * 20)
	// To test this, change value in zk and the
	// new value should be logged.
	ret = cfg.Get("profile.grepsr.from_email")
	log.Info(ret)
	time.Sleep(time.Second * 20)
	// Change root/path itself.
	ret = cfg.Get("aws.s3.access_key", "", "/phoenix/dev/phoenix-workers")
	log.Info(ret)
}
