package logger_test

import (
	"testing"

	log "bitbucket.org/grepsr/golib/logger"
)

func TestLogger(t *testing.T) {
	log.Initialize()
	log.Debug("This is a %s message.", "debug")
	log.Info("This is an %s message.", "info")
	log.Warn("This is a %s message.", "warning")
	log.Error("This is an %s message.", "error")
	log.Debug("")
}
