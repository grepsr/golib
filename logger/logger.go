package logger

import (
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

// LogFormatter is the custom formatter for logs.
type LogFormatter struct {
}

// Format method.
func (f *LogFormatter) Format(entry *log.Entry) ([]byte, error) {
	formattedTime := entry.Time.Format("2006-01-02 15:04:05.000")
	formattedTime = strings.Replace(formattedTime, ".", ",", 1)

	messageLiteral := entry.Message
	if len(messageLiteral) == 0 {
		messageLiteral = "N/A"
	}

	message := fmt.Sprintf(
		"%s - %s - %s - %s\n",
		formattedTime,
		entry.Data["env"],
		strings.ToUpper(entry.Level.String()),
		messageLiteral,
	)

	return []byte(message), nil
}

var contextLogger *log.Entry
var env string
var logLevelsMap = map[string]string{
	"10": "DEBUG",
	"20": "INFO",
	"30": "WARNING",
	"40": "ERROR",
}

func init() {
	Initialize()
}

// Debug message logger.
func Debug(message string, args ...interface{}) {
	contextLogger.Debugf(message, args...)
}

// Info message logger.
func Info(message string, args ...interface{}) {
	contextLogger.Infof(message, args...)
}

// Warn message logger.
func Warn(message string, args ...interface{}) {
	contextLogger.Warnf(message, args...)
}

// Error message logger.
func Error(message string, args ...interface{}) {
	contextLogger.Errorf(message, args...)
}

// Initialize is a proxy for init for testing purposes.
func Initialize() {
	env = getenv("APP_ENV", "dev")
	log.SetFormatter(new(LogFormatter))
	log.SetOutput(os.Stderr)
	setLogLevel()
	contextLogger = log.WithFields(log.Fields{
		"env": env,
	})
}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func setLogLevel() {
	level := getenv("APP_LOG_LEVEL", "")
	switch level {
	case "10":
		log.SetLevel(log.DebugLevel)
	case "20":
		log.SetLevel(log.InfoLevel)
	case "30":
		log.SetLevel(log.WarnLevel)
	case "40":
		log.SetLevel(log.ErrorLevel)
	default:
		if env == "prd" {
			log.SetLevel(log.InfoLevel)
		} else {
			log.SetLevel(log.DebugLevel)
		}
	}
}
