package event_test

import (
	"fmt"
	"os"
	"testing"

	"bitbucket.org/grepsr/golib/event"
)

func TestEvent(t *testing.T) {
	ev := event.New(
		os.Getenv("CFG_AWS_ACCESS_KEY_ID"),
		os.Getenv("CFG_AWS_SECRET_ACCESS_KEY"),
		os.Getenv("CFG_AWS_REGION"),
		os.Getenv("CFG_AWS_SNS_TOPIC_ARN"),
	)
	ret, err := ev.Emit("system", int(0), "UserCreatedEvent", "JSON", map[string]interface{}{
		"email":       "bidhan@grepsr.com",
		"first_name":  "Bidhan",
		"golang_test": true,
	}, 0, os.Getenv("SERVICE_NAME"), "golib")
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(ret)
	}
}
