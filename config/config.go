package config

import (
	"os"
	"strings"
	"time"

	log "bitbucket.org/grepsr/golib/logger"
	cnf "github.com/olebedev/config"
	"github.com/samuel/go-zookeeper/zk"
)

const maxConfigDepth = 3

var conn *zk.Conn
var conf *cnf.Config
var stopWatchChan = make(chan bool)

func createConnection(hosts []string) *zk.Conn {
	c, _, err := zk.Connect(hosts, time.Second)
	if err != nil {
		log.Error("Error connecting to zookeeper hosts.")
		log.Error(err.Error())
		os.Exit(1)
	}
	return c
}

// Get value for a key.
func Get(key string, args ...interface{}) string {
	defaultValue, root, logKey := processArgs(args)
	count := strings.Count(key, ".") + 1
	if count > maxConfigDepth {
		log.Warn("Simplify config setting '%s' by limiting depth to %d levels from current depth of %d.",
			key,
			maxConfigDepth,
			count,
		)
	}

	if len(root) != 0 {
		// Load new config
		log.Info("Root changed. Loading new config.")
		loadConfig(root, false)
	}

	ret, err := conf.String(key)
	if err != nil {
		log.Error(err.Error())
		ret = defaultValue
	}

	if logKey {
		log.Info("Getting config; key: %s, value: %s, default: %s, root: %s.",
			key, ret, defaultValue, root)
	}
	return ret
}

// Initialize is used for setting things up.
func Initialize(hosts []string, root string) {
	conn = createConnection(hosts)
	// Load initial config
	loadConfig(root, true)

}

// loadConfig loads config from zookeeper.
func loadConfig(path string, initial bool) {
	d, _, err := conn.Get(path)
	if err != nil {
		log.Error("Could not load config.")
		log.Error(err.Error())
	}
	parseConfig(string(d))

	// If the loading is not initial, stop
	// the current watcher before starting off a
	// new watcher goroutine.
	if !initial {
		log.Info("Stop watching for changes in current path.")
		stopWatchChan <- true
	}

	// Start off a watcher goroutine that watches for
	// changes in the path.
	go watchConfig(path)

}

// watchConfig watches for changes in the path and
// syncs the config if it has changed.
func watchConfig(path string) {
	for {
		log.Info("Config loaded. Now watching for changes.")
		_, _, events, err := conn.GetW(path)
		if err != nil {
			return
		}

		select {
		case evt := <-events:
			if evt.Type == zk.EventNodeDataChanged {
				log.Info("Config changed. Syncing now.")
				if d, _, err := conn.Get(path); err == nil {
					parseConfig(string(d))
				}
			}
		case <-stopWatchChan:
			return
		}

	}
}

// parseConfig parses the yaml config.
func parseConfig(configStr string) {
	c, err := cnf.ParseYaml(configStr)
	if err != nil {
		log.Error("Could not parse config.")
		log.Error(err.Error())
	}
	conf = c
}

// processArgs processes the arguments passed to Get.
func processArgs(args []interface{}) (defaultValue string, root string, logKey bool) {

	switch len(args) {
	case 0:
		break
	case 1:
		defaultValue, _ = args[0].(string)
	case 2:
		defaultValue, _ = args[0].(string)
		root, _ = args[1].(string)
	case 3:
		defaultValue, _ = args[0].(string)
		root, _ = args[1].(string)
		logKey, _ = args[2].(bool)
	}

	return
}
